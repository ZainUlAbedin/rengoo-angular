import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { map } from 'rxjs/operators';

// const httpOptions = {
//     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable()
export class EnergySoldService {
    SoldEnergyMap:any = 'https://hyperledger-power-watson-rest-v12-fearless-genet.eu-gb.mybluemix.net/api/User/';
    urlSellEnergy = 'https://hyperledger-power-watson-rest-v12-fearless-genet.eu-gb.mybluemix.net/api/addEnergyProduction';
    constructor(private http:HttpClient) {}

    sellEnergy(data: any) : Observable<HttpResponse<any>> {
        return this.http.post<any>(this.urlSellEnergy, data,
        {
            headers:new HttpHeaders().append('Content-Type','application/json')
        });
    }

    getEnergySold() {
        return this.http.get(this.SoldEnergyMap+'5bcd8c59a1393d25f9c768cf', {
            headers:new HttpHeaders().append('Content-Type','application/json')
          }).pipe(map(res =>res));

    }
}
