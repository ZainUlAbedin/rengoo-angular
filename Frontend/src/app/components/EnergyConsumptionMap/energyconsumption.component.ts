import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GoogleMapsAPIWrapper, AgmMap, LatLngBounds, LatLngBoundsLiteral,AgmCoreModule, MapsAPILoader} from '@agm/core';

import {EnergyConsumptionService} from '../../services/energyconsumption.service'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

 declare const google: any;

@Component({
  selector : 'app-energyconsumption',
  templateUrl : './energyconsumption.component.html',
  styleUrls: ['./energyconsumption.component.css']
})


export class EnergyConsumptionComponent {
  @ViewChild('AgmMap') agmMap: AgmMap;
  welcomeTxt: string = `Welcome to party!`;
  lat: number = 51.678418;
 lng: number = 7.809007;
distance: String;
users$: Object;
map = null;
latitudedata =[];
mapData = [];


selectedOption: string;
markers = [];
constructor(
            private _energyconsump: EnergyConsumptionService
            
          ) { }

  ngOnInit() {



   






      this._energyconsump.getEnergyConsumption().subscribe(datas => {
  
        

           this.latitudedata = datas['energyConsumption']
         console.log("this.latituddddddedata",this.latitudedata);
     
      
       this.getMarkersAndLoadMap(this.latitudedata);
      
      });





    }

    arrayMap(obj, fn){
      var aray = [];
      for(var i = 0; i < obj.length; i++)
      {
             aray.push(fn(obj[i], i));
     }
               return aray;

      }

    mapReady(map){
      const bounds: LatLngBounds = new google.maps.LatLngBounds();
      for (const mm of this.markers) {
        if(mm.lat != 0 && mm.lng != 0)
        console.log("markddddder",mm)
        bounds.extend(new google.maps.LatLng(mm.latitude, mm.longitude));
      }

      map.fitBounds(bounds);
    }

    ngAfterViewInit() {
    }



    getMarkersAndLoadMap(latsnlongs){
      this.markers = latsnlongs;
      console.log("my Makrer",this.markers)
    }













    mapIdle() {
       console.log('idle');
     }



clickedMarker(users$){
     console.log("fff");

     const nyc = new google.maps.LatLng(users$.lat, users$.lng);
       const london = new google.maps.LatLng(47.381714, 8.568479);
       const distance = google.maps.geometry.spherical.computeDistanceBetween(nyc, london);
       const d = distance/1000
       const r = Math.floor(d)
       this.distance = r+ " "+"km"
       console.log(d +" "+ "Km away");


}

icon = {
          url: 'http://localhost:4200/assets/img/geopower.png',
        
}


onMouseOver(infoWindow, gm) {

        if (gm.lastOpen != null) {
          console.log("not open")
            gm.lastOpen.close();
        }

        gm.lastOpen = infoWindow;


        infoWindow.open();
        console.log("open")
    }


onClickInfoView(users$){
  console.log("my new user lat is" + " "+ users$.lat)

}


}
