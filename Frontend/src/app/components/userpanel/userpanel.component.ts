import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { EnergySoldService } from '../../services/energySold.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import * as alertify from 'alertify.js';

@Component({
  selector: 'app-userpanel',
  templateUrl: './userpanel.component.html',
  styleUrls: ['./userpanel.component.css']
})
export class UserpanelComponent implements OnInit {

  

 Rangoo:String = 'https://hyperledger-power-watson-rest-appreciative-bear.eu-gb.mybluemix.net/';

  sellForm: FormGroup;
  sellModel : any = {
    user : '',
    energyResource: '',
    productionCapacity: 0,
    production: 0
  }

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private energySellService: EnergySoldService, private spinnerService: Ng4LoadingSpinnerService) {

  }

  ngOnInit() {



    this.sellForm = this.formBuilder.group({
      energyResource: ['WIND_POWER', [
        Validators.required
      ]],
      productionCapacity: ['', [
        Validators.required,
        Validators.pattern('[+-]?([0-9]*[.])?[0-9]+')
      ]]
    })
  }

  sell() {
    this.spinnerService.show();
    this.authService.getProfile().subscribe(res => {
      this.sellModel.user = `waltson.poc.hyperledger.User#${ res["user"]._id}`;
      this.sellModel.energyResource = this.sellForm.value.energyResource;
      this.sellModel.productionCapacity = parseInt(this.sellForm.value.productionCapacity);
      this.energySellService.sellEnergy(this.sellModel).subscribe(res => {
        alertify.logPosition('top right').success('Sucessfully Added!');    
        this.spinnerService.hide();
      }, err => {
        alertify.logPosition('top right').error('Error Occured! Please try again later.');    
        this.spinnerService.hide();
      })
    }, err => {
      alertify.logPosition('top right').error('Error Occured! Please try again later.');    
      this.spinnerService.hide();
    });

  }

}
